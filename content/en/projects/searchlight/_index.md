---
title: Searchlight
description: DARPA SEARCHLIGHT on MergeTB
weight: 10
---

{{% blocks/section type="section" %}}

# [Projects](../) / Searchlight

Technologies developed for the
[DARPA Searchlight](https://www.darpa.mil/program/searchlight)
program were evaluated on Merge testbeds and technologies.

This page contains information about papers and software in support of
the Searchlight program.

{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.1145/3565476.3569097" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.1145/3546096.3546103" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.1145/3546096.3546107" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.1109/EuroSPW55150.2022.00046" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.1145/3474718.3474730" >}}
{{% /blocks/section %}}

{{% blocks/section type="section" %}}
{{< bibentry "10.1145/3474718.3474721" >}}
{{% /blocks/section %}}

