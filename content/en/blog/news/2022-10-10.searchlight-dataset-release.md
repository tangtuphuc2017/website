---
date: 2022-10-10
title: "Now Available: The DARPA SEARCHLIGHT Dataset of Application Network Traffic"
linkTitle: "Now Available: The DARPA SEARCHLIGHT Dataset"
description:
author: Calvin Ardi
---

The DARPA SEARCHLIGHT dataset of application network traffic, consisting
of ~750GB of packet captures from ~2000 systematically conducted
experiments, is now available with an accompanying website to help
discoverability at:

  <https://mergetb.org/projects/searchlight/dataset/>

For more information about the dataset, please see our [research
paper](/projects/searchlight/#10.1145/3546096.3546103) of the same
title.

The tools we developed in creating this dataset are open source and
freely available at <https://gitlab.com/mergetb/exptools>.

#### acknowledgements

This research was developed with funding from the Defense Advanced
Research Projects Agency (DARPA). Work by USC/ISI was sponsored by
Sandia National Laboratories (SNL) under PO2160586. SNL is a
multimission laboratory managed and operated by National Technology &
Engineering Solutions of Sandia, LLC for the U.S. Department of Energy’s
National Nuclear Security Administration under contract DE-NA0003525.
The views, opinions and/or findings expressed are those of the author
and should not be interpreted as representing the official views or
policies of the Department of Defense, Department of Energy, or the U.S.
Government.

Searchlight Traffic Generation Dataset and Tools of Application Network
Traffic [DISTAR case 36697]

This document was cleared by DARPA on August 22, 2022. All copies should
carry the Distribution Statement “A” (Approved for Public Release,
Distribution Unlimited). This research was developed with funding from
the Defense Advanced Research Projects Agency (DARPA). The views,
opinions and/or findings expressed are those of the author and should
not be interpreted as representing the official views or policies of the
Department of Defense or the U.S. Government.
