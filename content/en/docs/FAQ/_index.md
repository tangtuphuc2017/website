---
title: "Frequently Asked Questions"
linkTitle: "Frequently Asked Questions (FAQ)"
weight: 99
description: Questions and their answers.
---

{{% pageinfo %}}
This section is under construction.
{{% /pageinfo %}}

## what xdc am i on? (lighthouse)

```bash
export XDC=$(uname -a | cut -d' ' -f2 | cut -d'-' -f1)
echo ${XDC}
```

## what materialization is my xdc connected to? (lighthouse)

```bash
export MERGE_MATERIALIZATION=$(head -1 /etc/resolv.conf | cut -d' ' -f2 | grep -Ev "cluster.local|test-cluster.redhat.com")
echo ${MERGE_MATERIALIZATION}
```

Result will be empty if your XDC is not connected to a materialization.

## Ansible error: "Message: Cannot write to ControlPath $HOME/.ansible/cp."

Check to make sure the `.ansible` directory exists in your home
directory, and that its owner and group is you (not root):

```bash
# check if the directory exists and its ownership
$ ls -ald ~/.ansible

# create the directory if it does not exist
$ mkdir ~/.ansible

# change the user and group ownership of directory to yourself
$ sudo chown -R $(id -u):$(id -g) ~/.ansible
```

Then re-run your `ansible` commands.
