---
title: "Command Line Interface Reference"
linkTitle: "CLI Reference"
weight: 3
description: >
  Using the Merge command line interface
---

{{% alert title="Tips" color="primary" %}}

- All commands support **abbreviated syntax**, so the following are equivalent

```sh
mrg list experiments
```

```sh
mrg li e
```

- As of version `v1.1.1`, the `mrg` binary supports self updating via `mrg update binary`.

- Most commands support **rendering as JSON** via the `--json` flag. This is useful
  for tool and script integration.

- All **names have a hierarchical syntax**, so an experiment called `muffins` in the
  project `bakery` is referred to as `muffins.bakery`

- There are **useful help menus** with all commands accessible through the `-h`
  flag.

- **Be sure to login first** `mrg login <username> <password>`, login tokens do
  not last forever and may expire so you may have to login again from time to
  time.
  {{% /alert %}}

## Experiments

### Listing

```
mrg list experiments
```

Example:

```sh
$ mrg list exp
Name.Project          Description    Mode
------------          -----------    ----
experiment.project                   Public
exp.glawler                          Public
bbb.glawler                          Public
eee.glawler                          Public
```

### Showing

```sh
mrg show experiment <name>.<project>
```

Usage:

```sh
Show an experiment

Usage:
  mrg show experiment <experiment>.<project> [flags]

Flags:
  -h, --help          help for experiment
  -m, --with-models   Save models for each revision
  -S, --with-status   Show compilation status for each revision

```

Example:

```sh
$ mrg show experiment experiment.project
Repo: https://git.dorvan.mergetb.net/project/experiment
Mode: Public
Description:
Creator: glawler
Maintainers:
Realizations:
  Revision                                    Realizations
  --------                                    ------------
  9eabb626da475f542bf4002cc2d9d1c448f6cafa    rlz.experiment.project
  d3da9590bbc8bedcb6c494ca6d84b57edb1aa9d6    (none)
```

## Models

A model is an experiment topology and is read from an experiment's git repository. The
model syntax is documented [here]({{<ref "model-ref" >}}).

### Compiling

A model can be compiled before pushing to an experiment repository via the `compile` command.
If successful the compiled model will be printed to standard out. On failure the
compilation errors will.

Note that this does not push the model to the experiment repository. It is just informational.
To push use `mrg push` or `git push`.

```sh
Compile a model. Return success or compile error stack on failure.

Usage:
  mrg compile <model-file> [flags]

Flags:
  -h, --help    help for compile
  -q, --quiet   Do not show the compiled model on success.

```

Successful example (`-q` argument is given to quiet output)

```sh
glawler@river:~$ mrg compile -q model.py
glawler@river:~$ echo $?
0
```

Unsuccessful example:

```sh
glawler@river:~$ mrg compile model.py
Title:        MX failed to compile
Detail:       Compiliation Failure
Evidence:     Exception: 'Network' object has no attribute 'Connect'

Type:          https://mergetb.org/errors/mx-failed-to-compile
Timestamp:     2022-02-14 20:04:11.560308413 +0000 UTC m=+5998.900641627
```

### Pushing

A model can be pushed to an experiment via `git` or `mrg`. To push via `mrg` use the
`mrg push ...` command.

```sh
Push a model to the given experiment repository.

Usage:
  mrg push <model-file> <experiment>.<project> [flags]

Flags:
      --branch string   Repository branch to push the model to. (default "master")
  -h, --help            help for push
      --tag string      Tag the commit with the given tag.
```

For example to push the model in `./model.py` to the master branch in experiment
`hello.murphy`:

```sh
mrg push ./model.py hello.murphy
Push succeeded. Revision: fb90c2aa1a19a07410d39ad373c842e7ee544586
```

If `--tag` is given the revision will be tagged with the tgiven tag:

```sh
mrg push ./model.py hello.murphy --tag v1.2.1
```

This creates a git tag on the revision and can be used to
realize the model.

{{% alert title="Note" color="warning" %}}
Athough the `push` usage has a `branch` arugment, the command only supports pushing to
the `master` branch. Pushing to a non-master branch will be in a future release.
{{% /alert %}}

## Realizations

### Realizing

```sh
mrg realize <realization>.<experiment>.<project> revision|tag <reference> [flags]
```

Here `<reference>` is the git revision or git tag of the source repository you would like to
realize. For example to create the realization "henry" with a revision tagged "olive" in
the experiment `hello` in the project `murphy`:

```sh
mrg realize henry.hello.murphy tag olive
```

The same but using the revision:

```sh
mrg realize henry.hello.murphy revision bbd67f119672bc0cedc8fd97476e6aeea7458a6c
```

### Listing

To list all realizations you have access to use `list `.

```sh
mrg list realizations
```

Example:

```sh
$ mrg list realizations
Realization               Nodes    Links    Status
-----------               -----    -----    ------
rlz.experiment.project    5        2        Succeeded
```

### Relinquishing

Relinquishing frees all resources held by a realization. If there is an active
materialization associated with the realization it will be dematerialized
before the realization is relinquished.

```sh
mrg relinquish <realization>.<experiment>.<project>
```

### Showing

```sh
mrg show realization <realization>.<experiment>.<project>
```

### Generating configs

`mrg generate` allows you to generate configurations from a realization, including INI style Ansible
inventories and etchosts-style name-to-IP mappings:

```sh
mrg generate inventory <realization>.<experiment>.<project>
```

```sh
mrg generate etchosts -p exp- <realization>.<experiment>.<project>
```

## Materializations

### Listing

```sh
mrg list materializations
```

### Showing

```sh
mrg show materialization <realization>.<experiment>.<project>
```

## XDCs

### Listing

```sh
mrg list xdcs
```

### Attaching

```sh
mrg xdc attach <xdc>.<project> <realization>.<experiment>.<project>
```

### Detaching

```sh
mrg xdc detach <xdc>.<project>
```
