---
title: "Getting Started"
linkTitle: "Getting Started"
weight: 1
description: >
  Getting setup to use Merge
---

This guide covers the basic things you need to do to get started with Merge.

{{% alert title="Attention" color="warning" %}}
This guide assumes you are using the reference portal at `mod.deterlab.net`. If you are using a
different portal, substitute addresses in this document that reference `mod.deterlab.net` accordingly.
Consult your project leader if you are not sure of the portal address.
{{% /alert %}}

{{% alert title="Tip" color="info" %}}
The Merge Portal uses its own identity server to manage user identities. This is much like having
a Google or Gitlab account, but in a server created for Merge Portals. This identity server manages
your identity data and authenticates your communications to the Merge Portal. Note that the account
made here is _not_ a Merge Portal account. It is an identity account. Later in this walk-through
portal admins will use this identity information to create a portal account.

In the future Merge will support third-party identity allowing merge accounts to be created using,
for example, github or google identities.
{{% /alert %}}

## Account Setup

The first step to experimenting on a Merge testbed is to create an account on the Merge portal.
Follow the instructions either <a href="#account-creation-through-launch">using "Launch"</a> (the graphical
web interface) or through the <a href="#account-creation-through-the-cli">Merge command line
interface (CLI)</a>.

### Account Creation through Launch

Navigate to the Merge "Launch" GUI at <a
href="https://launch.mod.deterlab.net">https://launch.mod.deterlab.net</a>. You'll be greeted with a page
that looks much like this

![](../hello-world-gui/01-login.png)

Click the `Register new account` link to load the account creation page.

![](../hello-world-gui/02-new-account.png)

Fill in your account details:

- `E-Mail` - the email of the account
- `Password` - the account password. The password will be rejected with a reason if it is not strong enough.
- `traits-username` - the username you will use to login to Merge

{{% alert title="Note" color="info" %}}
The password you choose will be used to login to Launch _and_ the Merge command line utility `mrg`.
If you do not use a command line aware password manager, you may want to choose an easier-to-type
password as you will be typing it in on the command line.
{{% /alert %}}

![](../hello-world-gui/03-new-murphy-account.png)

Once done, click `Sign Up`.

{{% alert title="Note" color="info" %}}
You may get redirected to a confirmation page that shows the account information. If so, just go back to the original portal launch page.
For DeterLab, this is https://launch.mod.deterlab.net.
{{% /alert %}}

### Activate Your Merge Portal Account

Send a notification to the Merge portal administrators to have your account created and activated. They will use the Identity account created above to create an account for you on the Merge Portal. Speak with your project leader for up-to-date information on how best to contact them.

Until your account is activated, you'll see that you do not have access to any projects on the portal. You can login, but you will not have access to anything so pages in Launch will be content-free.

![](../hello-world-gui/04-no-access.png)

Once your account has been activated, your data will load and you are ready to start using Merge!

### Account Creation through the CLI

First, download the Merge CLI `mrg` for the operating system and CPU
architecture of your machine at the <a href="https://gitlab.com/mergetb/portal/cli/-/releases/permalink/latest">latest release page</a>.

### Configuring the API endpoint

The `mrg` CLI needs to be configured to point to the GRPC API endpoint of the Merge portal. The
address can be constructed by prepending "grpc." to the portal address; e.g.,:

```shell
mrg config set server grpc.mod.deterlab.net
```

As noted above, consult your project leader for the appropriate portal address.

## Register account

Then, register your account using the `mrg register` command. For example, to create a user named
"murphy" with email "murphy@mergetb.org" and password "muffins1701":

```shell
mrg register murphy murphy@mergetb.org muffins1701
```

## Account Approval

Once you have created an account using either the web interface or the CLI, you should send a
notification to the Merge portal administrators to have your account approved. Speak with your
project leader for up-to-date information on how best to contact them.

Once your account has been approved, you are ready to start using Merge!
