#
#
#
#
#
#
#
# this list is managed manually.
#
# KEEP SORTED BY FIRST LISTED AUTHOR LASTNAME, THEN YEAR
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# ENTRY TEMPLATE:
#
# - id:
#   author:
#   sortdate:
#   title:
#   citation:
#   doi:
#   urlpdf:
#   urlcode:
#   urldata:
#   urlpresentation:
#   abstract:
#   bibtex: |-
#
#

references:
- id: 10.1145/3474718.3474721
  author: Ardi
  sortdate: 2021-08-09
  title: Building Reproducible Video Streaming Traffic Generators
  citation: "Calvin Ardi, Alefiya Hussain, and Stephen Schwab. 2021.
    Building Reproducible Video Streaming Traffic Generators. In Cyber
    Security Experimentation and Test Workshop (CSET '21). Association for
    Computing Machinery, New York, NY, USA, 91–95. DOI:
    `10.1145/3474718.3474721`."
  doi: 10.1145/3474718.3474721
  urlpdf: "https://dl.acm.org/doi/pdf/10.1145/3474718.3474721"
  urlcode: "https://gitlab.com/mergetb/exptools/video-streaming"
  abstract: Video streaming traffic dominates Internet traffic. However,
    there is a dearth of tools to generate such traffic on
    emulation-based testbeds. In this paper we present tools to create
    representative and reproducible video streaming traffic to evaluate
    the next generation of traffic classification, Quality of Service
    (QoS) algorithms and traffic engineering systems. We discuss 27
    different combinations of streaming video traffic types in this
    preliminary work, and illustrate the diversity of network-level
    dynamics in these protocols.
  bibtex: |-
    @inproceedings{10.1145/3474718.3474721,
    author    = {Ardi, Calvin and Hussain, Alefiya and Schwab, Stephen},
    title     = {Building Reproducible Video Streaming Traffic Generators},
    year      = 2021,
    month     = aug,
    isbn      = {9781450390651},
    publisher = {Association for Computing Machinery},
    address   = {New York, NY, USA},
    url       = {https://doi.org/10.1145/3474718.3474721},
    doi       = {10.1145/3474718.3474721},
    abstract  = {Video streaming traffic dominates Internet traffic.
        However, there is a dearth of tools to generate such traffic on
        emulation-based testbeds. In this paper we present tools to
        create representative and reproducible video streaming traffic
        to evaluate the next generation of traffic classification,
        Quality of Service (QoS) algorithms and traffic engineering
        systems. We discuss 27 different combinations of streaming video
        traffic types in this preliminary work, and illustrate the
        diversity of network-level dynamics in these protocols.},
    booktitle = {Cyber Security Experimentation and Test Workshop},
    pages     = {91–95},
    numpages  = {5},
    location  = {Virtual, CA, USA},
    series    = {CSET '21}
    }

- id: 10.1145/3546096.3546103
  author: Ardi
  sortdate: 2022-08-08
  title: The DARPA SEARCHLIGHT Dataset of Application Network Traffic
  citation: "Calvin Ardi, Connor Aubry, Brian Kocoloski, Dave DeAngelis,
    Alefiya Hussain, Matt Troglia, and Stephen Schwab. 2022. The DARPA
    SEARCHLIGHT Dataset of Application Network Traffic. In Proceedings of
    the 15th Workshop on Cyber Security Experimentation and Test (CSET
    '22). Association for Computing Machinery, New York, NY, USA, 59–64.
    DOI: `10.1145/3546096.3546103`."
  doi: 10.1145/3546096.3546103
  urlpdf: "/projects/searchlight/Ardi22a.pdf"
  urldata: "https://mergetb.org/projects/searchlight/dataset/"
  abstract: Researchers are in constant need of reliable data to
    develop and evaluate AI/ML methods for networks and cybersecurity.
    While Internet measurements can provide realistic data, such
    datasets lack ground truth about application flows. We present a ∼
    750GB dataset that includes ∼ 2000 systematically conducted
    experiments and the resulting packet captures with video streaming,
    video teleconferencing, and cloud-based document editing
    applications. This curated and labeled dataset has bidirectional and
    encrypted traffic with complete ground truth that can be widely used
    for assessments and evaluation of AI/ML algorithms.
  bibtex: |-
    @inproceedings{10.1145/3546096.3546103,
    author    = {Ardi, Calvin and Aubry, Connor and Kocoloski, Brian and
        DeAngelis, Dave and Hussain, Alefiya and Troglia, Matt and Schwab,
        Stephen},
    title     = {The DARPA SEARCHLIGHT Dataset of Application Network Traffic},
    year      = 2022,
    month     = aug,
    isbn      = {9781450396844},
    publisher = {Association for Computing Machinery},
    address   = {New York, NY, USA},
    url       = {https://doi.org/10.1145/3546096.3546103},
    doi       = {10.1145/3546096.3546103},
    abstract  = {Researchers are in constant need of reliable data to
        develop and evaluate AI/ML methods for networks and cybersecurity.
        While Internet measurements can provide realistic data, such
        datasets lack ground truth about application flows. We present a ∼
        750GB dataset that includes ∼ 2000 systematically conducted
        experiments and the resulting packet captures with video streaming,
        video teleconferencing, and cloud-based document editing
        applications. This curated and labeled dataset has bidirectional and
        encrypted traffic with complete ground truth that can be widely used
        for assessments and evaluation of AI/ML algorithms.},
    booktitle = {Proceedings of the 15th Workshop on Cyber Security Experimentation and Test},
    pages     = {59–64},
    numpages  = {6},
    keywords  = {datasets, network experimentation, network traffic},
    location  = {Virtual, CA, USA},
    series    = {CSET '22}
    }

- id: 10.1145/3565476.3569097
  author: Ardi
  sortdate: 2022-12-09
  title: Improving Fidelity in Video Streaming Experimentation on Testbeds with a CDN
  citation: "Calvin Ardi, Alefiya Hussain, Michael Collins, and Stephen
    Schwab. 2022. Improving Fidelity in Video Streaming Experimentation
    on Testbeds with a CDN. In Workshop on Design, Deployment, and
    Evaluation of Network-assisted Video Streaming (ViSNext '22),
    December 9, 2022, Roma, Italy. Association for Computing Machinery,
    New York, NY, USA, 1–7. DOI: `10.1145/3565476.3569097`"
  doi: 10.1145/3565476.3569097
  urlpdf: "/projects/searchlight/Ardi22b.pdf"
  abstract: Video streaming is the leading network traffic on the
    Internet, yet there are few tools to run high fidelity experiments
    with video streaming traffic on network emulation-based testbeds. In
    this paper, we present a framework to enable higher fidelity and
    principled experimentation with 36 different video streaming traffic
    scenario combinations that can be configured and deployed on a
    notional CDN and data metrics infrastructure. This framework can be
    used to further study and experiment with adaptive bitrate algorithms
    and other AI/ML solutions for video delivery.
  bibtex: |-
      @inproceedings{10.1145/3565476.3569097,
        author = {Ardi, Calvin and Hussain, Alefiya and Collins, Michael and Schwab, Stephen},
        title = {Improving Fidelity in Video Streaming Experimentation on Testbeds with a CDN},
        year = {2022},
        isbn = {9781450399364},
        publisher = {Association for Computing Machinery},
        address = {New York, NY, USA},
        url = {https://doi.org/10.1145/3565476.3569097},
        doi = {10.1145/3565476.3569097},
        abstract = {Video streaming is the leading network traffic on
            the Internet, yet there are few tools to run high fidelity
            experiments with video streaming traffic on network
            emulation-based testbeds. In this paper, we present a framework
            to enable higher fidelity and principled experimentation with 36
            different video streaming traffic scenario combinations that can
            be configured and deployed on a notional CDN and data metrics
            infrastructure. This framework can be used to further study and
            experiment with adaptive bitrate algorithms and other AI/ML
            solutions for video delivery.},
        booktitle = {Proceedings of the 2nd International Workshop on
            Design, Deployment, and Evaluation of Network-Assisted Video
            Streaming},
        pages = {1–7},
        numpages = {7},
        keywords = {network experimentation, content distribution
            network, network traffic, video streaming applications},
        location = {Rome, Italy},
        series = {ViSNext '22}
      }

- id: 10.1109/EuroSPW55150.2022.00046
  author: Collins
  sortdate: 2022-06-10
  title: Towards an Operations-Aware Experimentation Methodology
  citation: "M. Collins, A. Hussain and S. Schwab. 2022. Towards an
    Operations-Aware Experimentation Methodology. In 2022 IEEE European
    Symposium on Security and Privacy Workshops (EuroS&PW), 2022, pp.
    384-393, DOI: `10.1109/EuroSPW55150.2022.00046`."
  doi: 10.1109/EuroSPW55150.2022.00046
  urlpdf: "https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9799335"
  abstract: Security Operations Centers (SOCs) serve a critical role in
    protecting enterprise networks and systems. Despite this critical
    role, only a limited number of researchers in the field have an
    awareness of the obstacles and challenges in applying cyber ranges and
    cybersecurity testbeds to the area of SOC training, exercises and
    evaluation. This paper introduces a systematic approach to
    incorporating SOCs into cybersecu-rity experiments, including both
    training and evaluation. We present a reference SOC model, an
    implementation of that model and downloadable software distributions
    suitable for deploying on cyber ranges, and guidance towards a
    methodol-ogy to promote rigorous experiments including those involving
    human cyber operators. Metrics focused on analyst event load are
    presented in the context of measuring the impact of new threats,
    technologies and procedures on SOC performance. Collectively, these
    contributions serve as a basis for future work to engage the research
    and operational communities to work together to advance the
    state-of-the-art of SOC technologies and SOC operators.
  bibtex: |-
    @INPROCEEDINGS{10.1109/EuroSPW55150.2022.00046,
      author = {M. Collins and A. Hussain and S. Schwab},
      booktitle = {2022 IEEE European Symposium on Security and Privacy Workshops (EuroS&amp;PW)},
      title = {Towards an Operations-Aware Experimentation Methodology},
      year = {2022},
      volume = {},
      issn = {},
      pages = {384-393},
      abstract = {Security Operations Centers (SOCs) serve a critical
        role in protecting enterprise networks and systems. Despite this
        critical role, only a limited number of researchers in the field
        have an awareness of the obstacles and challenges in applying
        cyber ranges and cybersecurity testbeds to the area of SOC
        training, exercises and evaluation. This paper introduces a
        systematic approach to incorporating SOCs into cybersecu-rity
        experiments, including both training and evaluation. We present a
        reference SOC model, an implementation of that model and
        downloadable software distributions suitable for deploying on
        cyber ranges, and guidance towards a methodol-ogy to promote
        rigorous experiments including those involving human cyber
        operators. Metrics focused on analyst event load are presented in
        the context of measuring the impact of new threats, technologies
        and procedures on SOC performance. Collectively, these
        contributions serve as a basis for future work to engage the
        research and operational communities to work together to advance
        the state-of-the-art of SOC technologies and SOC operators.},
      keywords = {training;measurement;systematics;software;computer security;load modeling},
      doi = {10.1109/EuroSPW55150.2022.00046},
      url = {https://doi.ieeecomputersociety.org/10.1109/EuroSPW55150.2022.00046},
      publisher = {IEEE Computer Society},
      address = {Los Alamitos, CA, USA},
      month = jun
    }

- id: 10.1145/3546096.3546107
  author: DeAngelis
  sortdate: 2022-08-08
  title: Generating Representative Video Teleconferencing Traffic
  citation: "David DeAngelis, Alefiya Hussain, Brian Kocoloski, Calvin Ardi, and Stephen Schwab. 2022.
    Generating Representative Video Teleconferencing Traffic. In Cyber
    Security Experimentation and Test Workshop (CSET '22). Association for
    Computing Machinery, New York, NY, USA, 91–95. DOI:
    `10.1145/3546096.3546107`."
  doi: 10.1145/3546096.3546107
  urlpdf: "/projects/searchlight/DeAngelis22a.pdf"
#  urlpdf: "https://dl.acm.org/doi/pdf/10.1145/3546096.3546107"
  urlcode: "https://gitlab.com/mergetb/exptools/video-teleconference"
  abstract: Video teleconferencing (VTC) is a dominant network application,
    yet there is a dearth of tools to generate such traffic for systematic and
    reproducible experimentation. We present a framework to create
    representative video teleconferencing traffic and discuss our methodology
    for behavioral control of multiple bots to create human-like dialog
    coordination, including interactive talking and silence patterns. Our
    framework can be coupled with proprietary commercial VTC applications as
    well as deployed completely within a testbed environment to benchmark
    emerging networking technology and evaluate the next generation of traffic
    classification, quality of service (QoS) algorithms, and traffic
    engineering systems. 
  bibtex: |-
    @inproceedings{10.1145/3546096.3546107,
    author = {DeAngelis, David and Hussain, Alefiya and Kocoloski, Brian and
        Ardi, Calvin and Schwab, Stephen},
    title = {Generating Representative Video Teleconferencing Traffic},
    year = {2022},
    isbn = {9781450396844},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    url = {https://doi.org/10.1145/3546096.3546107},
    doi = {10.1145/3546096.3546107},
    abstract = {Video teleconferencing (VTC) is a dominant network
        application, yet there is a dearth of tools to generate such traffic
        for systematic and reproducible experimentation. We present a framework
        to create representative video teleconferencing traffic and discuss our
        methodology for behavioral control of multiple bots to create
        human-like dialog coordination, including interactive talking and
        silence patterns. Our framework can be coupled with proprietary
        commercial VTC applications as well as deployed completely within a
        testbed environment to benchmark emerging networking technology and
        evaluate the next generation of traffic classification, quality of
        service (QoS) algorithms, and traffic engineering systems.},
    booktitle = {Cyber Security Experimentation and Test Workshop},
    pages = {100–104},
    numpages = {5},
    keywords = {video teleconference, VoIP, network traffic generation,
        cybersecurity testbeds},
    location = {Virtual, CA, USA},
    series = {CSET 2022}
    }

- id: "238258"
  author: Goodfellow
  sortdate: 2019-08-12
  title: The DComp Testbed
  citation: "Ryan Goodfellow, Stephen Schwab, Erik Kline, Lincoln
    Thurlow, and Geoff Lawler. 2019. The DComp Testbed. In Cyber
    Security Experimentation and Test Workshop (CSET '19)."
  urlpdf: "https://www.usenix.org/system/files/cset19-paper_goodfellow.pdf"
  urlpresentation: "https://www.usenix.org/sites/default/files/conference/protected-files/cset19_slides_goodfellow.pdf"
  abstract: The DComp Testbed effort has built a large-scale testbed,
    combining customized nodes and commodity switches with modular
    software to launch the Merge open source testbed ecosystem. Adopting
    EVPN routing, DCompTB employs a flexible and highly adaptable strategy
    to provision network emulation and infrastructure services on a
    per-experiment basis. Leveraging a clean separation of the experiment
    creation process into realization at the Merge portal and
    materialization on the DCompTB site, the testbed implementation
    embraces modularity throughout. This enables a well-defined
    orchestration system and an array of reusable modular tools to perform
    all essential functions of the DCompTB. Future work will evaluate the
    robustness, performance and maintainability of this testbed design as
    it becomes heavily used by research teams to evaluate opportunistic
    edge computing prototypes.
  bibtex: |-
    @inproceedings {238258,
    author    = {Ryan Goodfellow and Stephen Schwab and Erik Kline and Lincoln Thurlow and Geoff Lawler},
    title     = {The {DComp} Testbed},
    booktitle = {12th USENIX Workshop on Cyber Security Experimentation
    and Test (CSET 19)},
    year      = {2019},
    address   = {Santa Clara, CA},
    url       = {https://www.usenix.org/conference/cset19/presentation/goodfellow},
    publisher = {USENIX Association},
    month     = aug,
    }
- id: 10.48550/arxiv.1810.08260
  author: Goodfellow
  sortdate: 2018-10-18
  title: "Merge: An Architecture for Interconnected Testbed Ecosystems"
  citation: "Ryan Goodfellow, Lincoln Thurlow, Srivatsan Ravi. 2018. Merge: An
  Architecture for Interconnected Testbed Ecosystems. arXiv:1810.08260. DOI:
  `10.48550/ARXIV.1810.08260`."
  doi: 10.48550/ARXIV.1810.08260
  urlpdf: "https://arxiv.org/pdf/1810.08260"
  urlcode: "https://gitlab.com/mergetb"
  abstract: In the cybersecurity research community, there is no
    one-size-fits-all solution for merging large numbers of
    heterogeneous resources and experimentation capabilities from
    disparate specialized testbeds into integrated experiments. The
    current landscape for cyber-experimentation is diverse, encompassing
    many fields including critical infrastructure, enterprise IT,
    cyber-physical systems, cellular networks, automotive platforms, IoT
    and industrial control systems. Existing federated testbeds are
    constricted in design to predefined domains of applicability,
    lacking the systematic ability to integrate the burgeoning number of
    heterogeneous devices or tools that enable their effective use for
    experimentation. We have developed the Merge architecture to
    dynamically integrate disparate testbeds in a logically centralized
    way that allows researchers to effectively discover, and use the
    resources and capabilities provided the by evolving ecosystem of
    distributed testbeds for the development of rigorous and
    high-fidelity cybersecurity experiments.
  bibtex: |-
    @misc{10.48550/arxiv.1810.08260,
    doi       = {10.48550/ARXIV.1810.08260},
    url       = {https://arxiv.org/abs/1810.08260},
    author    = {Goodfellow, Ryan and Thurlow, Lincoln and Ravi, Srivatsan},
    keywords  = {Distributed, Parallel, and Cluster Computing (cs.DC), FOS: Computer and information sciences, FOS: Computer and information sciences},
    title     = {Merge: An Architecture for Interconnected Testbed Ecosystems},
    publisher = {arXiv},
    year      = {2018},
    copyright = {arXiv.org perpetual, non-exclusive license},
    abstract  = "In the cybersecurity research community, there is no
    one-size-fits-all solution for merging large numbers of heterogeneous
    resources and experimentation capabilities from disparate specialized
    testbeds into integrated experiments. The current landscape for
    cyber-experimentation is diverse, encompassing many fields including
    critical infrastructure, enterprise IT, cyber-physical systems, cellular
    networks, automotive platforms, IoT and industrial control systems. Existing
    federated testbeds are constricted in design to predefined domains of
    applicability, lacking the systematic ability to integrate the burgeoning
    number of heterogeneous devices or tools that enable their effective use for
    experimentation. We have developed the Merge architecture to dynamically
    integrate disparate testbeds in a logically centralized way that allows
    researchers to effectively discover, and use the resources and capabilities
    provided the by evolving ecosystem of distributed testbeds for the
    development of rigorous and high-fidelity cybersecurity experiments.",
    }
- id: 10.1145/3474718.3474730
  author: Kocoloski
  sortdate: 2021-08-09
  title: Case Studies in Experiment Design on a Minimega Based Network
      Emulation Testbed
  citation: "Brian Kocoloski, Alefiya Hussain, Matthew Troglia, Calvin
    Ardi, Steven Cheng, Dave DeAngelis, Christopher Symonds, Michael
    Collins, Ryan Goodfellow, and Stephen Schwab. 2021. Case Studies in
    Experiment Design on a minimega Based Network Emulation Testbed. In
    Cyber Security Experimentation and Test Workshop (CSET '21).
    Association for Computing Machinery, New York, NY, USA, 83–90. DOI:
    `10.1145/3474718.3474730`."
  doi: 10.1145/3474718.3474730
  urlpdf: "https://cset21.isi.edu/papers/cset21-12.pdf"
  urlcode: "https://gitlab.com/mergetb/exptools/minimega-cset-2021"
  urlpresentation: "https://cset21.isi.edu/cset21-12-pres.pdf"
  abstract: This paper describe our team’s experience using minimega, a
    network emulation system using node and network virtualization, to
    support evaluation of a set of networked and distributed systems for
    topology discovery, traffic classification and engineering in the
    DARPA Searchlight program. We present the methodology we developed
    to encode network and traffic definitions into an experiment
    description model, and how our tools compile this model onto the
    underlying minimega API. We then present three cases studies which
    demonstrate the ability of our EDM to support experiments with
    diverse network topologies, diverse traffic mixes, and networks with
    specialized layer-2 connectivity requirements. We conclude with the
    overall takeaways from using minimega to support our evaluation
    process.
  bibtex: |-
    @inproceedings{10.1145/3474718.3474730,
    author    = {Kocoloski, Brian and Hussain, Alefiya and Troglia, Matthew and
    Ardi, Calvin and Cheng, Steven and DeAngelis, Dave and Symonds,
    Christopher and Collins, Michael and Goodfellow, Ryan and Schwab,
    Stephen},
    title     = {Case Studies in Experiment Design on a Minimega Based Network
    Emulation Testbed},
    year      = 2021,
    isbn      = {9781450390651},
    publisher = {Association for Computing Machinery},
    address   = {New York, NY, USA},
    url       = {https://doi.org/10.1145/3474718.3474730},
    doi       = {10.1145/3474718.3474730},
    abstract  = {This paper describe our team’s experience using minimega, a
        network emulation system using node and network virtualization,
        to support evaluation of a set of networked and distributed
        systems for topology discovery, traffic classification and
        engineering in the DARPA Searchlight program. We present the
        methodology we developed to encode network and traffic
        definitions into an experiment description model, and how our
        tools compile this model onto the underlying minimega API. We
        then present three cases studies which demonstrate the ability
        of our EDM to support experiments with diverse network
        topologies, diverse traffic mixes, and networks with specialized
        layer-2 connectivity requirements. We conclude with the overall
        takeaways from using minimega to support our evaluation
        process.},
    booktitle = {Cyber Security Experimentation and Test Workshop},
    pages     = {83–90},
    numpages  = {8},
    location  = {Virtual, CA, USA},
    series    = {CSET '21}
    }
- id: 10.1109/infcomw.2019.8845249
  author: Thurlow
  sortdate: 2019-04-29
  title: "Sled: System-Loader for Ephemeral Devices"
  citation: "Lincoln Thurlow, Ryan Goodfellow and Stephen Schwab. 2019.
    Sled: System-Loader for Ephemeral Devices. IEEE INFOCOM 2019 - IEEE
    Conference on Computer Communications Workshops (INFOCOM WKSHPS),
    2019, pp. 913-914, DOI: `10.1109/INFCOMW.2019.8845249`."
  doi: 10.1109/INFCOMW.2019.8845249
  urlcode: "https://gitlab.com/mergetb/tech/sled"
  urlpdf: "/projects/papers/Thurlow19a.pdf"
  abstract: |-
    Imaging an operating system onto a server is an extensive and time
    consuming process, which commonly taking several minutes to boot. For
    a testbed administrator, loading an image onto a device is one of the
    slowest yet most relied upon tasks that a testbed must complete prior
    to starting an experiment. To minimize wait times for disk loading, as
    well as to streamline the process of image loading, we introduce Sled,
    a system-loader for ephemeral devices, that uses warm-booting to
    quickly image devices. Sled is able to provision a new operating
    system in under a minute, and is 10 times faster than previous methods
    of image loading.
  bibtex: |-
    @INPROCEEDINGS{8845249,
    author    = {Thurlow, Lincoln and Goodfellow, Ryan and Schwab, Stephen},
    booktitle = {IEEE INFOCOM 2019 - IEEE Conference on Computer Communications Workshops (INFOCOM WKSHPS)},
    title     = {Sled: System-Loader for Ephemeral Devices},
    year      = 2019,
    volume    = {},
    number    = {},
    pages     = {913-914},
    doi       = {10.1109/INFCOMW.2019.8845249}
    }
